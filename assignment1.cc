

#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/network-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/flow-monitor-helper.h"
#include "ns3/ipv4-flow-classifier.h"

// Default Network Topology
//
//              10.1.1.0
//       node0 ---------- node1
// 
//              10.1.2.0
//       node2 ---------- node3
//

using namespace ns3;

//NS_LOG_COMPONENT 
NS_LOG_COMPONENT_DEFINE("FirstScriptExample");

int
main(int argc, char* argv[])
{

    //Parsing degli argomenti dell'esempio passati
    CommandLine cmd(__FILE__);
    cmd.Parse(argc, argv);

    Time::SetResolution(Time::NS); 
    //LogComponentEnable("UdpEchoClientApplication", LOG_LEVEL_INFO); 
    //LogComponentEnable("UdpEchoServerApplication", LOG_LEVEL_INFO);

    
    NodeContainer nodes;
    nodes.Create(4);

    PointToPointHelper pointToPoint;
    pointToPoint.SetDeviceAttribute("DataRate", StringValue("5Mbps"));
    pointToPoint.SetChannelAttribute("Delay", StringValue("2ms"));

    //modello di canale con perdita di pacchetti
    Ptr<RateErrorModel> errorModel = CreateObject<RateErrorModel> ();
    errorModel->SetAttribute ("ErrorRate", DoubleValue (0.001)); 
    pointToPoint.SetDeviceAttribute ("ReceiveErrorModel", PointerValue(errorModel));


    NetDeviceContainer devices01;
    NetDeviceContainer devices23;

    /*Collega il nodo 0 con il nodo 1 e il nodo 2 con il nodo 3*/
    devices01 = pointToPoint.Install(nodes.Get (0), nodes.Get (1));
    devices23 = pointToPoint.Install(nodes.Get (2), nodes.Get (3));


    InternetStackHelper stack;
    stack.Install(nodes);

    /*Assegna indirizzi IP ai dispositivi*/
    Ipv4AddressHelper address;

    address.SetBase("10.1.1.0", "255.255.255.0");
    Ipv4InterfaceContainer interfaces01 = address.Assign (devices01);

    address.SetBase ("10.1.2.0", "255.255.255.0");
    Ipv4InterfaceContainer interfaces23 = address.Assign (devices23);

    /*Creazione dell'applicazione client-server Echo*/
    UdpEchoServerHelper echoServer(9);

    //Installa echo server sul nodo 1
    ApplicationContainer serverApps01 = echoServer.Install(nodes.Get(1));
    serverApps01.Start(Seconds(1.0));
    serverApps01.Stop(Seconds(20.0));

    //Installa echo server sul nodo 3
    ApplicationContainer serverApps23 = echoServer.Install(nodes.Get(3));
    serverApps23.Start(Seconds(1.0));
    serverApps23.Stop(Seconds(20.0));

    // Creazione dell'applicazione EchoClient sul nodo 0
    UdpEchoClientHelper echoClient01(interfaces01.GetAddress(1), 9);
    echoClient01.SetAttribute("MaxPackets", UintegerValue(5));
    echoClient01.SetAttribute("Interval", TimeValue(Seconds(1.0)));
    echoClient01.SetAttribute("PacketSize", UintegerValue(1024));

    ApplicationContainer clientApps01 = echoClient01.Install(nodes.Get(0));
    clientApps01.Start(Seconds(2.0));
    clientApps01.Stop(Seconds(30.0));

    // Installa echoClient sul nodo 2
    UdpEchoClientHelper echoClient23(interfaces23.GetAddress(1), 9);
    echoClient23.SetAttribute ("MaxPackets", UintegerValue (10));
    echoClient23.SetAttribute ("Interval", TimeValue (Seconds (1.0)));
    echoClient23.SetAttribute ("PacketSize", UintegerValue (1024));

    ApplicationContainer clientApps23 = echoClient23.Install(nodes.Get(2));
    clientApps23.Start (Seconds (2.0));
    clientApps23.Stop (Seconds (40.0));

    // Monitora con flowMonitor
    FlowMonitorHelper flowM;
    Ptr<FlowMonitor> monitor = flowM.InstallAll();

    Simulator::Stop(Seconds(30));
    Simulator::Run();
  

    // Report del FlowMonitor
    monitor->CheckForLostPackets();
    Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier>(flowM.GetClassifier());
    std::map<FlowId, FlowMonitor::FlowStats> stats = monitor->GetFlowStats();

    
    for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator i = stats.begin(); i != stats.end(); ++i) {
    Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow(i->first);
    std::cout << "Flow ID: " << i->first << " (" << t.sourceAddress << " -> " << t.destinationAddress << ")\n";
    std::cout << "  Tx Bytes: " << i->second.txBytes << "\n";
    std::cout << "  Rx Bytes: " << i->second.rxBytes << "\n";
    std::cout << "  Throughput: " << i->second.rxBytes * 8.0 / (i->second.timeLastRxPacket.GetSeconds () - i->second.timeFirstTxPacket.GetSeconds ()) / 1024 / 1024 << " Mbps\n";
    std::cout << "  Delay: " << i->second.delaySum.GetSeconds() / i->second.rxPackets << " s\n";
    std::cout << "  rx Packets: " << i->second.rxPackets  << " \n";
    std::cout << "  tx Packets: " << i->second.txPackets << " \n";
    std::cout << "  Packet Loss: " << i->second.lostPackets << "\n";
}

  // Termina la simulazione
  Simulator::Destroy ();
  return 0;
}

